Views Exposed Form Exclude
===========

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers

INTRODUCTION
------------

The Views Exposed Form Exclude module provides a way for site builders to choose
what view filters or sorts to excluded on the exposed form block. An example use
case would be that you need to show the View sort filters in a different region,
which are separate from the comparison filters.



REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

After completing the installation, follow the instructions below to configure
the View to use the Exposed Form Exclude block.

* Create or edit an existing View (e.g. `/admin/structure/views`)
* Under the `Exposed form` section within the View UI, click on `Yes/No` for the
  `Exposed form in block` option.
* Select `Yes` on the setting form, after that an additional option
  `Allow excluding sorts/filters` will be shown. Select `Yes` which will provide
  settings to exclude the filters/sorts on the block.
* Save the View.
* Navigate to the `Block layout` (e.g. `/admin/structure/block`)
* Click `Place block` for the region you would like to render the Views exposed
  form under.
* On the Views exposed form block settings you'll see an option to exclude
  either filters or sorts, only for ones that have been exposed on the View.
  Select all the filter or sorts to exclude.
