<?php

declare(strict_types=1);

namespace Drupal\views_exposed_form_exclude\Plugin\views\display_extender;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Annotation\ViewsDisplayExtender;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;

/**
 * Define the views expose form exclude display extender plugin.
 *
 * @ViewsDisplayExtender(
 *   id = "views_exposed_form_exclude",
 *   title = @Translation("Exposed Form Exclude")
 * )
 */
class ExcludeDisplayExtender extends DisplayExtenderPluginBase {

  /**
   * {@inheritDoc}
   */
  public function defineOptionsAlter(&$options): void {
    $options['exposed_block_exclude'] = [
      'default' => 0,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    $display_handler = $this->view->display_handler;
    if ($form_state->get('section') === 'exposed_block') {
      $form['exposed_block_exclude'] = [
        '#type' => 'radios',
        '#title' => $this->t('Allow excluding sorts/filters'),
        '#options' => [
          TRUE => $this->t('Yes'),
          FALSE => $this->t('No')
        ],
        '#states' => [
          'visible' => [
            ':input[name="exposed_block"]' => ['value' => 1]
          ]
        ],
        '#default_value' => $display_handler->getOption('exposed_block_exclude'),
      ];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state): void {
    $display_handler = $this->view->display_handler;
    if ($form_state->get('section') === 'exposed_block') {
      $display_handler->setOption(
        'exposed_block_exclude',
        $form_state->getValue('exposed_block_exclude')
      );
    }
  }
}
