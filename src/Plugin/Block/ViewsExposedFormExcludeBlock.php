<?php

declare(strict_types=1);

namespace Drupal\views_exposed_form_exclude\Plugin\Block;

use Drupal\views\ViewExecutable;
use Drupal\views\ViewExecutableFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\views\Plugin\Block\ViewsExposedFilterBlock;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the views exposed form exclude block.
 */
class ViewsExposedFormExcludeBlock extends ViewsExposedFilterBlock {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Block constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\views\ViewExecutableFactory $executable_factory
   *   The view executable factory.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The views storage.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The current user.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ViewExecutableFactory $executable_factory,
    EntityStorageInterface $storage,
    AccountInterface $user,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $executable_factory,
      $storage,
      $user
    );
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('views.executable'),
      $container->get('entity_type.manager')->getStorage('view'),
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'exclude' => [
        'sort' => [],
        'filter' => [],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['exclude'] = [
      '#type' => 'details',
      '#title' => $this->t('Exclude'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $exclude = $this->getConfiguration()['exclude'];

    if ($sort_options = $this->viewSortOptions()) {
      $form['exclude']['sort'] = [
        '#type' => 'select',
        '#title' => $this->t('Sort'),
        '#multiple' => TRUE,
        '#description' => $this->t(
          'Select what sorts should be excluded from the views exposed form.'
        ),
        '#options' => $sort_options,
        '#default_value' => $exclude['sort']
      ];
    }

    if ($filter_options = $this->viewFilterOptions()) {
      $form['exclude']['filter'] = [
        '#type' => 'select',
        '#title' => $this->t('Filter'),
        '#multiple' => TRUE,
        '#description' => $this->t(
          'Select what filters should be excluded from the views exposed form.'
        ),
        '#options' => $filter_options,
        '#default_value' => $exclude['filter']
      ];
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['exclude'] = $form_state->getValue('exclude');
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $build = parent::build();

    $this->alterSortBuild($build);
    $this->alterFilterBuild($build);

    return $build;
  }

  /**
   * Alter views expose form filter build array.
   *
   * @param array $build
   *   The form render array.
   */
  protected function alterFilterBuild(array &$build): void {
    foreach ($this->getExclude('filter') as $element_id) {
      if (!isset($build[$element_id])) {
        continue;
      }
      $build[$element_id]['#access'] = FALSE;
    }
  }

  /**
   * Define the sort elements.
   *
   * @return string[]
   *   An array of sort elements.
   */
  protected function sortElements(): array {
    return ['sort_by', 'sort_order'];
  }

  /**
   * Alter views expose form sort build array.
   *
   * @param array $build
   *   The form render array.
   */
  protected function alterSortBuild(array &$build): void {
    foreach ($this->getExclude('sort') as $key) {
      if (isset($build['sort_by']['#options'][$key])) {
        unset($build['sort_by']['#options'][$key]);

        if (empty($build['sort_by']['#options'])) {
          foreach ($this->sortElements() as $element) {
            if (!isset($build[$element])) {
              continue;
            }
            $build[$element]['#access'] = FALSE;
          }
        }
      }

      if (
        $this->moduleHandler->moduleExists('better_exposed_filters')
        && isset($build['sort_bef_combine']['#options'])
        && !empty($build['sort_bef_combine']['#options'])
      ) {
          foreach (['ASC', 'DESC'] as $order) {
            unset($build['sort_bef_combine']['#options']["{$key}_$order"]);
          }

          if (empty($build['sort_bef_combine']['#options'])) {
            unset($build['sort_bef_combine']);
          }
        }
    }
  }

  /**
   * Get the view sort options.
   *
   * @return array
   *   An array of view sort options.
   */
  protected function viewSortOptions(): array {
    $options = [];
    $view = $this->getView();

    foreach ($view->sort as $sort) {
      $expose = $sort->options['expose'] ?? [];
      if (
        !$sort instanceof SortPluginBase
        || !isset($expose['field_identifier'], $expose['label'])
        || !$sort->isExposed()
      ) {
        continue;
      }
      $options[$expose['field_identifier']] = $expose['label'];
    }
    ksort($options);

    return $options;
  }

  /**
   * Get the view filter options.
   *
   * @return array
   *   An array of view filter options.
   */
  protected function viewFilterOptions(): array {
    $options = [];
    $view = $this->getView();

    foreach ($view->filter as $filter) {
      $expose = $filter->exposedInfo();
      if (
        !$filter instanceof FilterPluginBase
        || !isset($expose['value'], $expose['label'])
        || !$filter->isExposed()
      ) {
        continue;
      }
      $options[$expose['value']] = $expose['label'];
    }
    ksort($options);

    return $options;
  }

  /**
   * Get the fully built view executable.
   *
   * @return \Drupal\views\ViewExecutable
   *   The view executable instance.
   */
  protected function getView(): ViewExecutable {
    $view = $this->view;
    $view->build($this->displayID);

    return $view;
  }

  /**
   * Get views exposed exclude options.
   *
   * @param string $type
   *   The exclude type, (e.g. sort, filter)
   *
   * @return array
   *   An array of the exclude options.
   */
  protected function getExclude(string $type): array {
    return $this->getConfiguration()['exclude'][$type] ?? [];
  }
}
